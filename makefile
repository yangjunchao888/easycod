tag = easycode:test

port = 8080
args=
id=easycode
os = linux
arch = amd64

gbuild:
        GOOS=$(os) GOARCH=$(arch) go build -v -o build/bin/easycode
dbuild:
        docker build -f build/docker/Dockerfile -t $(tag) .

log:
        docker logs $(id)
clear:
        rm -rf build/bin/*
deploy:
        docker pull dockerhub.com/yangjc/easycode:test
        sudo docker-compose up -d
restart:
        sudo docker-compose restart
stop:
        sudo docker-compose down