package main

import (
	"log"

	"gitlab.com/easycod/app"
)

func main() {
	log.Println("hello world")
	if app.Max(1, 2) != 2 {
		panic("bad method")
	}
}
